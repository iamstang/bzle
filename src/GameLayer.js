var GameLayer = cc.LayerColor.extend({
  init: function() {
    this._super( new cc.Color( 127, 127, 127, 255 ) );
    this.setPosition( new cc.Point( 0, 0 ) );

    initField();

    initBlock();

    this.timeLabel = cc.LabelTTF.create( " Time : 60" , 'Arial' , 30);
    this.timeLabel.setPosition( new cc.p( 200 , 500 ));
    this.addChild( this.timeLabel );



    this.addKeyboardHandlers();
    this.scheduleUpdate();

    return true;
  },

  initField : function(){
    this.field = new Field();
	  this.field.setPosition( cc.p( 0, 40 ) );
    this.addChild( this.field );
  },

  initBlock : function(){
    this.block = new Block();
    this.block.setPosition( cc.p( 405 , 500  ));
    this.addChild( this.block );
    this.block.setField( this.field );
    this.block.scheduleUpdate();
  }
  addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },

  onKeyDown: function( keyCode, event ) {
	     if ( keyCode == cc.KEY.right  ){
              this.block.moveRight();

        }else if ( keyCode == cc.KEY.left){
              this.block.moveLeft();

        }else if ( keyCode == cc.KEY.up){

        }else if ( keyCode == cc.KEY.down){
              this.block.moveDown();
              setTimeout(this.field.checkExplosion,1000);
              this.field.checkExplosion();


        }


    },




    onKeyUp: function( keyCode, event ) {

    },

    update: function(){
     if(time <= 0){
       time += 900;
       this.field.addMore();

     }else {
       time -= 1;
       this.field.checkExplosion();
       this.field.updateBoxDrop();
       this.field.updateField();
     }
     this.timeLabel.setString( "Time Left: " + Math.floor(time/60) );



    }
});
var StartScene = cc.Scene.extend({
  onEnter: function() {
    this._super();
    var layer = new GameLayer();
    layer.init();
    this.addChild( layer );
  }
});

var time = 900;
GameLayer.STATES = {
    FRONT: 1,
    STARTED: 2,
    END: 3
};
