var Field = cc.Node.extend({
    ctor: function() {
        this._super();
        this.HEIGHT = 10;
        this.WIDTH = 7;

        setTimeout(this.checkExplosion,1000);

        this.fieldArr = [];

        for (var i = 0 ; i < this.HEIGHT ; i++ ) {
          this.fieldArr[i] = [];

          for (var j = 0 ; j < this.WIDTH ; j++ ) {
            if ( i < 7 ){
              this.fieldArr[i][j] = 0 ;
            } else{
            this.fieldArr[i][j] = this.randomNumber();
            }
          }
        }
     },
  randomNumber: function(){
    var a = 1+Math.floor(Math.random()*5);
    return a;
  },

  remove: function(){
    this.removeAllChildren(true);
  },

  updateField: function(){
    this.cleanup();
    this.remove();


    for ( var r = 0; r < this.HEIGHT; r++ ) {
    for ( var c = 0; c < this.WIDTH; c++ ) {
        if ( this.fieldArr[ r ][ c ] == 0){
          var s = cc.Sprite.create( 'res/images/bg.png' );
        }else if ( this.fieldArr[ r ][ c ] == 1 ) {
          var s = cc.Sprite.create( 'res/images/whitebox.png' );

        } else if ( this.fieldArr[ r ][ c ] == 2 ) {
          var s = cc.Sprite.create( 'res/images/redbox.png' );

        } else if ( this.fieldArr[ r ][ c ] == 3 ) {
          var s = cc.Sprite.create( 'res/images/bluebox.png' );

        } else if ( this.fieldArr[ r ][ c ] == 4 ) {
          var s = cc.Sprite.create( 'res/images/greenbox.png' );

        } else if ( this.fieldArr[ r ][ c ] == 5 ) {
          var s = cc.Sprite.create( 'res/images/greybox.png' );
        }
        s.setAnchorPoint( cc.p( 0, 0 ) );
        s.setPosition( cc.p( 250 + c * 45, (this.HEIGHT - r - 1) * 45 ) );
        this.addChild( s );
        }
      }

      this.scheduleUpdate();
  },




  checkExplosion: function(){
    for (var i = 0 ; i < this.HEIGHT ; i++  ){
      for ( var j = 0 ; j < this.WIDTH ; j++){
        if (this.fieldArr[i][j]!=0){


            if ( this.checkRepBlock(i,j) == 1 ){
              // right block
              if (this.checkRight(i,j+1)) this.fieldArr[i][j+2]=0;
              if (this.checkUp(i,j+1)) this.fieldArr[i-1][j+1]=0;
              if (this.checkDown(i,j+1)) this.fieldArr[i+1][j+1]=0;
              // left block
              if (this.checkLeft(i,j-1)) this.fieldArr[i][j-2]=0;
              if (this.checkUp(i,j-1)) this.fieldArr[i-1][j-1]=0;
              if (this.checkDown(i,j-1)) this.fieldArr[i+1][j-1]=0;
              // down block
              if (this.checkLeft(i+1,j)) this.fieldArr[i+1][j-1]=0;
              if (this.checkRight(i+1,j)) this.fieldArr[i+1][j+1]=0;
              if (this.checkDown(i+1,j)) this.fieldArr[i+2][j]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i+1][j]=0;
              this.fieldArr[i][j-1]=0
              this.fieldArr[i][j+1]=0

            }

              else if ( this.checkRepBlock(i,j) == 2 ){

              //down block
              if (this.checkLeft(i+1,j)) this.fieldArr[i+1][j-1]=0;
              if (this.checkRight(i+1,j)) this.fieldArr[i+1][j+1]=0;
              if (this.checkDown(i+1,j)) this.fieldArr[i+2][j]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i-1][j]=0;
              this.fieldArr[i+1][j]=0;
            }

            else if ( this.checkRepBlock(i,j) == 3 ){
              // right block
              if (this.checkRight(i,j+1)) this.fieldArr[i][j+2]=0;
              if (this.checkUp(i,j+1)) this.fieldArr[i-1][j+1]=0;
              if (this.checkDown(i,j+1)) this.fieldArr[i+1][j+1]=0;
              // left block
              if (this.checkLeft(i,j-1)) this.fieldArr[i][j-2]=0;
              if (this.checkUp(i,j-1)) this.fieldArr[i-1][j-1]=0;
              if (this.checkDown(i,j-1)) this.fieldArr[i+1][j-1]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i][j-1]=0;
              this.fieldArr[i][j+1]=0;
            }
            else if ( this.checkRepBlock(i,j) == 4 ){
              // right block
              if (this.checkRight(i,j+1)) this.fieldArr[i][j+2]=0;
              if (this.checkUp(i,j+1)) this.fieldArr[i-1][j+1]=0;
              if (this.checkDown(i,j+1)) this.fieldArr[i+1][j+1]=0;

              // down block
              if (this.checkLeft(i+1,j)) this.fieldArr[i+1][j-1]=0;
              if (this.checkRight(i+1,j)) this.fieldArr[i+1][j+1]=0;
              if (this.checkDown(i+1,j)) this.fieldArr[i+2][j]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i+1][j]=0;
              this.fieldArr[i][j+1]=0;
            }
              else if ( this.checkRepBlock(i,j) == 5 ){
              // down block
              if (this.checkLeft(i+1,j)) this.fieldArr[i+1][j-1]=0;
              if (this.checkRight(i+1,j)) this.fieldArr[i+1][j+1]=0;
              if (this.checkDown(i+1,j)) this.fieldArr[i+2][j]=0;

              // left block
              if (this.checkLeft(i,j-1)) this.fieldArr[i][j-2]=0;
              if (this.checkUp(i,j-1)) this.fieldArr[i-1][j-1]=0;
              if (this.checkDown(i,j-1)) this.fieldArr[i+1][j-1]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i+1][j]=0;
              this.fieldArr[i][j-1]=0;
            }
          else if ( this.checkRepBlock(i,j) == 6 ){
              // right block
              if (this.checkRight(i,j+1)) this.fieldArr[i][j+2]=0;
              if (this.checkUp(i,j+1)) this.fieldArr[i-1][j+1]=0;
              if (this.checkDown(i,j+1)) this.fieldArr[i+1][j+1]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i-1][j]=0;
              this.fieldArr[i][j+1]=0;
            }
          else if ( this.checkRepBlock(i,j) == 7 ){
              // left block
              if (this.checkLeft(i,j-1)) this.fieldArr[i][j-2]=0;
              if (this.checkUp(i,j-1)) this.fieldArr[i-1][j-1]=0;
              if (this.checkDown(i,j-1)) this.fieldArr[i+1][j-1]=0;

              this.fieldArr[i][j]=0;
              this.fieldArr[i-1][j]=0;
              this.fieldArr[i][j-1]=0;

            }
          }
        }
      }
  },

  checkRepBlock: function( i , j ){


    if ( this.checkRight(i,j) && this.checkDown(i,j) && this.checkLeft(i,j)){
      return 1;
    }
    else if ( this.checkUp(i,j) && this.checkDown(i,j) ){
      return 2;
    }
    else if ( this.checkRight(i,j) && this.checkLeft(i,j) ){
      return 3;
    }
    else if ( this.checkRight(i,j) && this.checkDown(i,j) ){
      return 4;
    }
    else if ( this.checkLeft(i,j) && this.checkDown(i,j) ){
        return 5;
    }
    else if ( this.checkUp(i,j) && this.checkRight(i,j)){
        return 6;
    }
    else if (this.checkUp(i,j) && this.checkLeft(i,j)){
        return 7;
    }
      return 0;
  },

  updateBoxDrop: function(){
    for ( var i = 0 ; i < this.HEIGHT ; i++){
      for ( var j = 0 ; j < this.WIDTH ; j++){
        if (i == 9){
          break;
        } else {
            if ( (this.fieldArr[ i ][ j ]!= 0) && (this.fieldArr[ i +1 ][ j ] == 0) ){
              this.fieldArr[i+1][j] = this.fieldArr[i][j];
              this.fieldArr[i][j]=0;
              this.updateBoxDrop();
            }
          }
        }
      }
    },

  checkUp: function( row , col ){
    if ( row == 0 ) return false;

    if (this.fieldArr[ row ][ col ] == this.fieldArr[ row-1 ][ col ] ) return true;

      return false;
  },

  checkDown: function( row , col ){
    if ( row == 9 ) return false;

    if (this.fieldArr[ row ][ col ] == this.fieldArr[ row + 1 ][ col ] ) return true;
      return false;
  },

  checkRight: function( row , col ){
    if ( col == 6 ) return false;

    if (this.fieldArr[ row ][ col ] == this.fieldArr[ row ][ col + 1 ] ) return true;

      return false;
  },
  checkLeft: function( row , col){
    if ( col == 0 ) return false;

    if (this.fieldArr[row][col] == this.fieldArr[row][col-1] ) return true;
      return false;
  },

  checkEmptySpace: function ( col ){
   for ( var i = 9 ; i >= 0 ; i--){
     if ( this.fieldArr[i][col] != 0 ){

     } else {
       return i;
     }
   }
   return 0;
 },
  addToField: function ( col , row , model ){
   this.fieldArr[col][row] = model ;
 },

  addMore: function(){

    for ( var i = 0 ; i < this.HEIGHT ; i++ ){
      for ( var j = 0 ; j < this.WIDTH ; j++ ){

        if ( i == 9 ){
          var a = this.randomNumber();

          while ( a == this.fieldArr[i-1][j] ){
            a = this.randomNumber();
          }

          this.fieldArr[i][j] = a;
        }// else if ( this.fieldArr[i+1][j] != 0 )
          else {
          this.fieldArr[i][j] = this.fieldArr[i+1][j];
        }


      }
    }
  }


});
