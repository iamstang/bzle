var Block = cc.Sprite.extend({
    ctor: function() {
      this._super();
      this.updateModel();

    },

    updateModel: function( ){
      this.blockModel = this.randomNumber();
      if ( this.blockModel == 1 ) {
        this.initWithFile( 'res/images/whitebox.png' );

      } else if ( this.blockModel == 2 ) {
        this.initWithFile( 'res/images/redbox.png' );

      } else if ( this.blockModel == 3 ) {
        this.initWithFile( 'res/images/bluebox.png' );

      } else if ( this.blockModel == 4 ) {
        this.initWithFile( 'res/images/greenbox.png' );

      } else if ( this.blockModel == 5 ) {
        this.initWithFile( 'res/images/greybox.png' );
      }
    },

    update: function( dt ){
      this.scheduleUpdate();
    },

    randomNumber: function(){
      var a = 1+Math.floor(Math.random()*5);
      return a;
    },

    moveRight: function(){
      var pos = this.getPosition();
      if ( pos.x >= 500 ){

      } else {
        this.setPosition( new cc.p( pos.x + 45 , pos.y ));
      }

      this.update();
    },

    moveLeft: function(){
      var pos = this.getPosition();
      if ( pos.x <= 275){}
      else {
        this.setPosition( new cc.p( pos.x - 45 , pos.y));
      }

      this.update();
    },

    moveDown: function(){

      this.field.addToField(this.field.checkEmptySpace(this.checkRow()),this.checkRow(),this.blockModel);
      this.updateModel();

    },



    setField: function( field ){
      this.field = field;
    },

    checkRow: function(){
      var pos = this.getPosition();
      if ( pos.x > 495 ){
        return 6;
      } else if ( pos.x > 450 ){
        return 5;
      } else if ( pos.x > 405 ){
        return 4;
      } else if ( pos.x > 360 ){
        return 3;
      } else if ( pos.x > 315 ){
        return 2;
      } else if ( pos.x > 270 ){
        return 1;
      } else {
        return 0;
      }
    }

});
